(ns cljs.user
  (:require [blog.core]
            [blog.system :as system]))

(def go system/go)
(def reset system/reset)
(def stop system/stop)
(def start system/start)
