(ns blog.test-runner
  (:require
   [doo.runner :refer-macros [doo-tests]]
   [blog.core-test]
   [blog.common-test]))

(enable-console-print!)

(doo-tests 'blog.core-test
           'blog.common-test)
